package hello.model;

import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import java.io.Serializable;


@Document(collection = "orders")
public class Order implements Serializable {
    @Id
    private ObjectId id;
    @Field("user_id")
    private ObjectId userId;
    @Field("concrete_project")
    private ObjectId concreteProjectId;
    private String place;
    private String status;
    private Review review;

    public ObjectId getId() {
        return id;
    }

    public void setId(ObjectId id) {
        this.id = id;
    }

    public String getPlace() {
        return place;
    }

    public void setPlace(String place) {
        this.place = place;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public ObjectId getUserId() {
        return userId;
    }

    public void setUserId(ObjectId userId) {
        this.userId = userId;
    }

    public ObjectId getConcreteProjectId() {
        return concreteProjectId;
    }

    public void setConcreteProjectId(ObjectId concreteProjectId) {
        this.concreteProjectId = concreteProjectId;
    }

    public Review getReview() {
        return review;
    }

    public void setReview(Review review) {
        this.review = review;
    }
}
