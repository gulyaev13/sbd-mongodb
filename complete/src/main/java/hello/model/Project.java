package hello.model;

import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Document(collection = "projects")
public class Project implements Serializable {
    @Id
    private ObjectId id;
    private String name;
    private int length;
    private int width;
    private int area;
    @Field("floor_plans")
    private List<FloorPlan> floorPlans;
    @Field("concrete_projects")
    private List<ObjectId> concreteProjectsId;

    public ObjectId getId() {
        return id;
    }

    public void setId(ObjectId id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getLength() {
        return length;
    }

    public void setLength(int length) {
        this.length = length;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getArea() {
        return area;
    }

    public void setArea(int area) {
        this.area = area;
    }

    public List<FloorPlan> getFloorPlans() {
        return floorPlans;
    }

    public void setFloorPlans(List<FloorPlan> floorPlans) {
        this.floorPlans = floorPlans;
    }

    public boolean deleteLastFloorPlan(){
        if(floorPlans.size() <= 1){
            return false;
        }
        return Objects.nonNull(floorPlans.remove(floorPlans.size() - 1));
    }

    public List<ObjectId> getConcreteProjectsId() {
        return concreteProjectsId;
    }

    public void setConcreteProjectsId(List<ObjectId> concreteProjectsId) {
        this.concreteProjectsId = concreteProjectsId;
    }
    public void addConcreteProjectId(ObjectId concreteProjectId) {
        if(this.concreteProjectsId == null){
            this.concreteProjectsId = new ArrayList<>();
        }
        this.concreteProjectsId.add(concreteProjectId);
    }
}
