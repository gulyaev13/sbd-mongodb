package hello.service;

import com.google.gson.Gson;
import hello.model.*;
import hello.repository.ConcreteProjectRepository;
import hello.repository.OrderRepository;
import hello.repository.ProjectRepository;
import hello.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class DBService {
    @Autowired
    ConcreteProjectRepository concreteProjectRepository;
    @Autowired
    OrderRepository orderRepository;
    @Autowired
    ProjectRepository projectRepository;
    @Autowired
    UserRepository userRepository;

    public DBService(){}

    /**
     * Create methods
     */
    @CachePut(cacheNames="mongo_users", key="#username")
    public User createUser(String username, String password){
        User user = userRepository.save(new User(username, password));
        return user;
    }

    @CachePut(cacheNames="mongo_users_info", key="#username")
    public UserInfo createUserInfo(String username, String lastName, String firstName,
                                   String secondName, Date dateOfBirth,
                                   String sex, String address, String phoneNumber){
        User user = userRepository.findByUsername(username);
        if(user == null){
            throw new IllegalArgumentException("User: " + username + ": doesn't exist in database");
        }
        UserInfo userInfo = new UserInfo(lastName, firstName, secondName, dateOfBirth, sex, address, phoneNumber);
        user.setInfo(userInfo);
        userRepository.save(user);
        return userInfo;
    }


    public FloorPlan createFloorPlan(int height, int maxHeight, String imgUrl){
        Image image = new Image(imgUrl);
        FloorPlan floorPlan = new FloorPlan();
        floorPlan.setHeight(height);
        floorPlan.setMaxHeight(maxHeight);
        floorPlan.setImage(image);
        return floorPlan;
    }

    @CachePut(cacheNames = "mongo_projects", key="#planName")
    public Project createProject(String planName, int length, int width, int area, List<FloorPlan> floorPlans){
        Project project = new Project();
        project.setName(planName);
        project.setLength(length);
        project.setWidth(width);
        project.setArea(area);
        project.setFloorPlans(floorPlans);
        project = projectRepository.save(project);
        return project;
    }

    @CachePut(cacheNames = "mongo_concrete_projects", key="#concreteProjectName")
    public ConcreteProject createConcreteProject(String concreteProjectName, String projectName, String material, List<String> urls){
        ConcreteProject concreteProject = new ConcreteProject();
        Project project = projectRepository.findByName(projectName);
        if(project == null){
            throw new IllegalArgumentException("Project: " + projectName + ": doesn't exist in database");
        }
        concreteProject.setName(concreteProjectName);
        concreteProject.setMaterial(material);
        if(urls.isEmpty()){
            throw new IllegalArgumentException("Images for concrete project not found");
        }
        concreteProject.setImages(urls.stream().map(Image::new).collect(Collectors.toList()));
        concreteProjectRepository.save(concreteProject);
        concreteProject = concreteProjectRepository.findByName(concreteProjectName);
        project.addConcreteProjectId(concreteProject.getId());
        projectRepository.save(project);
        return concreteProject;
    }

    @CacheEvict(cacheNames = "mongo_orders", key="#username")
    public Order createOrder(String place, String username, String concreteProjectName){
        Order order = new Order();
        order.setStatus("ordered");
        order.setPlace(place);
        User user = userRepository.findByUsername(username);
        if(user == null){
            throw new IllegalArgumentException("User: " + username + ": doesn't exist in database");
        }
        ConcreteProject concreteProject = concreteProjectRepository.findByName(concreteProjectName);
        if(concreteProject == null){
            throw new IllegalArgumentException("Concrete project: " + concreteProjectName + ": doesn't exist in database");
        }
        order.setUserId(user.getId());
        order.setConcreteProjectId(concreteProject.getId());
        orderRepository.save(order);
        return orderRepository.findByUserIdAndPlace(user.getId(), place);
    }

    @CacheEvict(cacheNames = "mongo_reviews", key="#username")
    public Review createReview(String reviewText, String imageUrl, String place, String username){
        Review review = new Review();
        User user = userRepository.findByUsername(username);
        if(user == null){
            throw new IllegalArgumentException("User: " + username + ": doesn't exist in database");
        }
        Order order = orderRepository.findByUserIdAndPlace(user.getId(), place);
        if(order == null){
            throw new IllegalArgumentException("Order: by" + username+ " in " + place + ": doesn't exist in database");
        }
        if(!order.getStatus().equals("ready")){
            throw new IllegalStateException("Review can add to order with status \"ready\", ACTUAL: \""
                    + order.getStatus() + "\"");
        }
        Image image = new Image(imageUrl);
        review.setReviewDate(new Date());
        review.setReviewText(reviewText);
        review.setImage(image);
        order.setReview(review);
        orderRepository.save(order);
        return review;
    }

    /**
     * Read
     */
    @Cacheable(value="mongo_users", key="#username")
    public User readUserByUsername(String username){
        return userRepository.findByUsername(username);
    }

    @Cacheable(value="mongo_users_info", key="#username")
    public UserInfo readUserInfoByUsername(String username){
        User user = userRepository.findByUsername(username);
        return user.getInfo();
    }

    @Cacheable(value="mongo_projects", key="#name")
    public Project readProjectByName(String name){
        return projectRepository.findByName(name);
    }

    @Cacheable(value="mongo_concrete_projects", key="#name")
    public ConcreteProject readConcreteProjectByName(String name){
        return concreteProjectRepository.findByName(name);
    }

    @Cacheable(value="mongo_orders", key="#username")
    public List<Order> readOrdersByUsername(String username){
        User user = userRepository.findByUsername(username);
        return orderRepository.findAllByUserId(user.getId());
    }

    @Cacheable(value="mongo_reviews", key="#projectName")
    public List<Review> readReviewsByConcreteProjects(String projectName){
        ConcreteProject cp = concreteProjectRepository.findByName(projectName);
        return  orderRepository.findAllByConcreteProjectId(cp.getId()).stream().map(Order::getReview)
                .collect(Collectors.toList());
    }

    /**
     * delete
     *
     */
    @CacheEvict(value = "mongo_users", key = "#username")
    public void deleteUserByUsername(String username){
        userRepository.deleteByUsername(username);
    }

    @CacheEvict(value = "mongo_users_info", key = "#username")
    public void deleteUserInfoByUsername(String username){
        User user = userRepository.findByUsername(username);
        user.setInfo(null);
        userRepository.save(user);
    }

    @CacheEvict(value = "mongo_projects", key = "#name")
    public void deleteProjectByName(String name){
        projectRepository.deleteByName(name);
    }

    public void deleteLastFloorByName(String name){
        Project project = projectRepository.findByName(name);
        if(project != null){
            if(!project.deleteLastFloorPlan()){
                throw new IllegalStateException("Project " + name + " contains only one floor");
            }
            projectRepository.save(project);
        }
    }

    /**
     *
     * update
     */
    @CachePut(cacheNames="mongo_users", key="#username")
    public User updateUser(String username, String password, String newPassword){
        User user = userRepository.findByUsername(username);
        User updatedUser;
        if(user == null){
            throw new IllegalArgumentException("User: " + username + ": doesn't exist in database");
        }
        if(user.getPassword().equals(password)){
            user.setPassword(newPassword);
            updatedUser = userRepository.save(user);
        } else {
            throw new IllegalArgumentException("Old password not correct");
        }
        return updatedUser;
    }

    @CacheEvict(cacheNames = "mongo_orders", key="#username")
    public Order changeOrderStatus(String username, String place, String newStatus){
        User user = userRepository.findByUsername(username);
        if(user == null){
            throw new IllegalArgumentException("User: " + username + ": doesn't exist in database");
        }
        Order order = orderRepository.findByUserIdAndPlace(user.getId(), place);
        if(order == null){
            throw new IllegalArgumentException("Order: by" + username+ " in " + place + ": doesn't exist in database");
        }
        order.setStatus(newStatus);
        return orderRepository.save(order);
    }
}

