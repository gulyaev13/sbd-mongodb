package hello;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import hello.model.*;
import hello.repository.UserRepository;
import hello.service.DBService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

import static java.lang.System.exit;

@SpringBootApplication
public class Application implements CommandLineRunner {
	@Autowired
	UserRepository userRepository;
	@Autowired
	DBService dbService;

	private static final SimpleDateFormat sdf = new SimpleDateFormat("dd-mm-yyyy");

	private Scanner scanner;
	private final Gson gson = new GsonBuilder().setPrettyPrinting().create();

	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		scanner = new Scanner(System.in);
		exit:
		while (true){
			System.out.println("Операции:\n" +
					"\t1. Создать\n" +
					"\t2. Вывести\n" +
					"\t3. Обновить\n" +
					"\t4. Удалить\n" +
					"\t5. Выход");
			int menu;
			try{
				menu = Integer.parseUnsignedInt(scanner.nextLine());
			} catch (Exception ex){
				menu = 0;
			}
			switch (menu){
				case 1:
					createEntity();
					break;
				case 2:
					readEntity();
					break;
				case 3:
					updateEntity();
					break;
				case 4:
					deleteEntity();
					break;
				case 5:
					break exit;
				default:
					System.out.println("Неправильный элемент меню! Пожалуйста, выберите снова!");
			}
		}

		exit(0);
	}
	private void readEntity() {
		System.out.println("Выберите данные, которые хотите вывести:");
		System.out.println("\t1.  USERS");
		System.out.println("\t2.  USERS_INFO");
		System.out.println("\t3.  PROJECTS");
		System.out.println("\t4.  CONCRETE_PROJECTS");
		System.out.println("\t5.  ORDERS");
		System.out.println("\t6.  REVIEWS");
		int menu;
		try{
			menu = Integer.parseUnsignedInt(scanner.nextLine());
		} catch (Exception ex){
			menu = 0;
		}
		String result = null;
		switch (menu){
			case 1:
				try {
					System.out.println("Введите Логин:");
					String username = readLine("Логин");
					result = gson.toJson(dbService.readUserByUsername(username));
				} catch (Exception e){
					result = e.getMessage();
				}
				break;
			case 2:
				try {
					System.out.println("Введите Логин:");
					String username = readLine("Логин");
					result = gson.toJson(dbService.readUserInfoByUsername(username));
				} catch (Exception e){
					result = e.getMessage();
				}
				break;
			case 3:
				try {
					System.out.println("Введите название базового проекта:");
					String projectName = readLine("Название базового проекта");
					result = gson.toJson(dbService.readProjectByName(projectName));
				} catch (Exception e){
					result = e.getMessage();
				}
				break;
			case 4:
				try {
					System.out.println("Введите название конкретного проекта:");
					String projectName = readLine("Название конкретного проекта");
					result = gson.toJson(dbService.readConcreteProjectByName(projectName));
				} catch (Exception e){
					result = e.getMessage();
				}
				break;
			case 5:
				try {
					System.out.println("Введите параметр заказа:");
					String username = readLine("Логин заказчика");
					result = gson.toJson(dbService.readOrdersByUsername(username));
				} catch (Exception e){
					result = e.getMessage();
				}
				break;
			case 6:
				try {
					System.out.println("Введите параметр отзыва:");
					String projectName = readLine("Название конкретного проекта");
					result = gson.toJson(dbService.readReviewsByConcreteProjects(projectName));
				} catch (Exception e){
					result = e.getMessage();
				}
				break;
			default:
				result = "Неправильный элемент меню!";
		}
		System.out.println(result);
	}

	private void createEntity(){
		System.out.println("Выберите данные, которые хотите добавить:");
		System.out.println("\t1.  USERS");
		System.out.println("\t2.  USERS_INFO");
		System.out.println("\t3.  PROJECTS");
		System.out.println("\t4.  CONCRETE_PROJECTS");
		System.out.println("\t5.  ORDERS");
		System.out.println("\t6.  REVIEWS");
		int menu;
		try{
			menu = Integer.parseUnsignedInt(scanner.nextLine());
		} catch (Exception ex){
			menu = 0;
		}
		String result = null;
		switch (menu){
			case 1:
				try {
					System.out.println("Введите Логин и Пароль:");
					String username = readLine("Логин");
					String password = readLine("Пароль");
					User user = dbService.createUser(username, password);
					result = "Пользователь создан";
				} catch (Exception e){
					result = e.getMessage();
				}
				break;
			case 2:
				try {
					System.out.println("Введите Логин и Пароль:");
					String username     = readLine("Логин");
					String firstName    = readLine("Имя");
					String lastName     = readLine("Фамилия");
					String secondName   = readLine("Отчество");
					String dateOfBirth  = readLine("Дата рождения(dd-mm-yy)");
					String sex          = readLine("Пол");
					String address      = readLine("Адресс");
					String phoneNumber  = readLine("Номер телефона");
					UserInfo user = dbService.createUserInfo(username, firstName, lastName, secondName,
							sdf.parse(dateOfBirth), sex, address, phoneNumber);
					result = "Пользовательская информация создана";
				} catch (Exception e){
					result = e.getMessage();
				}
				break;
			case 3:
				try {
					System.out.println("Введите параметры проекта:");
					int floorNumber = Integer.parseUnsignedInt(readLine("Количество этажей(1-5)"));
					if(floorNumber < 1 || floorNumber > 5){
						result = "Неправильная высота здания";
						break;
					}
					List<FloorPlan> floorPlan = new ArrayList<>(5);
					for(int i = 0; i < floorNumber; ++i){
						System.out.println("Введите параметры " + i + " этажа");
						String imgUrl 	= readLine("URL картинки планировки");
						int height		= Integer.parseUnsignedInt(readLine("Начальная высота (см)"));
						int maxHeight 	= Integer.parseUnsignedInt(readLine("Конечная высота (см)"));
						floorPlan.add(dbService.createFloorPlan(height, maxHeight, imgUrl));
					}
					String planName = readLine("Название проекта");
					int length = Integer.parseUnsignedInt(readLine("Длина"));
					int width = Integer.parseUnsignedInt(readLine("Ширина"));
					int area = Integer.parseUnsignedInt(readLine("Площадь"));
					dbService.createProject(planName, length, width, area, floorPlan);
					result = "Проект создан";
				} catch (Exception e){
					result = e.getMessage();
				}
				break;
			case 4:
				try {
					System.out.println("Введите параметры конкретного проекта:");
					String name = readLine("Название конкретного проекта");
					String projectName = readLine("Название базового проекта");
					String material = readLine("Материал");
					int imgNumber = Integer.parseUnsignedInt(readLine("Количество изображений(1-5)"));
					if(imgNumber < 1 || imgNumber > 6){
						result = "Неправильное количество изображений";
						break;
					}
					List<String> urls = new ArrayList<>(6);
					for(int i = 0; i < imgNumber; ++i){
						urls.add(readLine("URL картинки проекта"));
					}
					ConcreteProject project = dbService.createConcreteProject(name, projectName, material,
							urls.stream().filter(s -> !s.isEmpty()).collect(Collectors.toList()));
					result = "Конкретный проект создан";
				} catch (Exception e){
					result = e.getMessage();
				}
				break;
			case 5:
				try {
					System.out.println("Введите параметры заказа:");
					String place = readLine("Место");
					String username = readLine("Логин заказчика");
					String projectName = readLine("Название конкретного проекта");
					dbService.createOrder(place, username, projectName);
					result = "Заказ создан";
				} catch (Exception e){
					result = e.getMessage();
				}
				break;
			case 6:
				try {
					System.out.println("Введите параметры отзыва:");
					String imgUrl = readLine("URL");
					String text = readLine("Текст отзыва");
					String place = readLine("Место");
					String username = readLine("Логин заказчика");

					dbService.createReview(text, imgUrl, place, username);
					result = "Заказ создан";
				} catch (Exception e){
					result = e.getMessage();
				}
				break;
			default:
				result = "Неправильный элемент меню!";
		}
		System.out.println(result);
	}

	private void deleteEntity() {
		System.out.println("Выберите таблицу, из которой удалить данные:");
		System.out.println("\t1.  Удалить из USERS");
		System.out.println("\t2.  Удалить из USERS_INFO");
		System.out.println("\t3.  Удалить из PROJECTS");
		System.out.println("\t4.  Удалить последний этаж из PROJECTS");
		int menu;
		try{
			menu = Integer.parseUnsignedInt(scanner.nextLine());
		} catch (Exception ex){
			menu = 0;
		}
		String result = null;
		switch (menu){
			case 1:
				try {
					System.out.println("Введите Логин:");
					String username = readLine("Логин");
					dbService.deleteUserByUsername(username);
					result = "Пользователь удалён";
				} catch (Exception e){
					result = e.getMessage();
				}
				break;
			case 2:
				try {
					System.out.println("Введите Логин:");
					String username = readLine("Логин");
					dbService.deleteUserInfoByUsername(username);
					result = "Информация пользователя удалена";
				} catch (Exception e){
					result = e.getMessage();
				}
				break;
			case 3:
				try {
					System.out.println("Введите название базового проекта:");
					String projectName = readLine("Название базового проекта");
					dbService.deleteProjectByName(projectName);
					result = "Проект удалён";
				} catch (Exception e){
					result = e.getMessage();
				}
				break;
			case 4:
				try {
					System.out.println("Введите название базового проектаб у которого нужно удалить последний этаж:");
					String projectName = readLine("Название базового проекта");
					dbService.deleteLastFloorByName(projectName);
					result = "Последний этаж удалён";
				} catch (Exception e){
					result = e.getMessage();
				}
				break;
			default:
				result = "Неправильный элемент меню!";
		}
		System.out.println(result);
	}

	private void updateEntity(){
		System.out.println("Выберите таблицу, в которую хотите изменить данные:");
		System.out.println("\t1.  Изменить пароль в USERS");
		System.out.println("\t2.  Изменить статус заказа в ORDERS");
		int menu;
		try{
			menu = Integer.parseUnsignedInt(scanner.nextLine());
		} catch (Exception ex){
			menu = 0;
		}
		String result = null;
		switch (menu){
			case 1:
				try {
					System.out.println("Введите Логин и Пароль:");
					String username = readLine("Логин");
					String password = readLine("Старый пароль");
					String newPassword = readLine("Новый пароль");
					dbService.updateUser(username, password, newPassword);
					result = "Пароль изменён";
				} catch (Exception e){
					result = e.getMessage();
				}
				break;
			case 2:
				try {
					System.out.println("Введите данные заказа:");
					String username = readLine("Логин");
					String place = readLine("Место");
					String newStatus = readLine("Новый статус");
					dbService.changeOrderStatus(username, place, newStatus);
					result = "Статус изменён";
				} catch (Exception e){
					result = e.getMessage();
				}
				break;
			default:
				result = "Неправильный элемент меню!";
		}
		System.out.println(result);
	}

	private String readLine(String prompt){
		System.out.print("\t" + prompt + ": ");
		return scanner.nextLine();
	}
}
