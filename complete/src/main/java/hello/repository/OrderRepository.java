package hello.repository;

import hello.model.Order;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

public interface OrderRepository extends MongoRepository<Order, Long> {
    List<Order> findAllByUserId(ObjectId userId);
    List<Order> findAllByConcreteProjectId(ObjectId concreteProjectId);
    Order findByUserIdAndPlace(ObjectId userId, String place);
    void deleteByUserId(ObjectId userId);
}
