package hello.repository;

import hello.model.ConcreteProject;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface ConcreteProjectRepository extends MongoRepository<ConcreteProject, Long> {
    ConcreteProject findByName(String name);

    void deleteByName(String name);
}