package hello.repository;

import hello.model.Project;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface ProjectRepository extends MongoRepository<Project, Long> {
    Project findByName(String name);
    void deleteByName(String name);
}